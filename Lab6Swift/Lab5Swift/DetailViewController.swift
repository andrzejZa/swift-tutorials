//
//  DetailViewController.swift
//  Lab5Swift
//
//  Created by Andrzej Z on 25.02.2018.
//  Copyright © 2018 stud. All rights reserved.
//

import UIKit



class DetailViewController: UIViewController {
    
    @IBOutlet var ImageView: UIImageView!
    @IBOutlet var label: UILabel!
    
    var imageName=""
    var restName=""
    override func viewDidLoad() {
        super.viewDidLoad()
        ImageView.image = UIImage(named: imageName)
        label.text = restName
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
