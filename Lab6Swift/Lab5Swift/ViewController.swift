//
//  ViewController.swift
//  Lab5Swift
//
//  Created by stud on 17.12.2017.
//  Copyright © 2017 stud. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDataSource, UITableViewDelegate  {
    @IBOutlet var tableView: UITableView!
    var restaurantNames = ["Pod papugami","Naleśniki babci","Bar Nadwaga","Hortyca","Restauracja pod gryfami", "Spiż","Kurna Chata","Przystanek Wrocek","Pierogarnia Smacznego", "Mega Pierożki", "Bar Narożniak", "Bar Borowik","Pizzeria Papu","Pizzeria Etna", "Restauracja Lviv","Pierogarnia", "Pijalnia vódli i piwa"]
    
     var restaurantLocations = ["Wrocław, Sukiennice","Wrocław, Fabryczna","Wrocław, Rynek","Wrocław","Wrocław", "Wrocław","Wrocław, Lotnisko","Wrocław", "Wrocław", "Wrocław, Mietalowców", "Wrocław","Wrocław","Wrocław","Wrocław","Wrocław","Wrocław","Wrocław"]
    
    var restaurantImages = ["barrafina","bourkestreetbakery","cafedeadend","cafeloisl","cafelore", "caskpubkitchen","confessional","donostia","fiveleaves", "forkeerestaurant", "grahamavenuemeats", "haighschocolate","homei","palominoespresso", "petiteoyster","posatelier", "royaloak"]
    
   
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return restaurantNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "Cell"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier , for: indexPath) as! RestaurantTableViewCell
        //cell.textLabel?.text = restaurantNames[indexPath.row]
        //cell.imageView?.image = UIImage(named: restaurantImages[indexPath.row])
        
        cell.nameLabel?.text = restaurantNames[indexPath.row]
        cell.thumbnailImageView?.image = UIImage(named: restaurantImages[indexPath.row])
         cell.locationLabel?.text = restaurantLocations[indexPath.row]
        return cell
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetails" {
            if let indexPath = tableView.indexPathForSelectedRow {
            let destController = segue.destination as! DetailViewController
              destController.imageName = restaurantImages[indexPath.row]
                destController.restName = restaurantNames[indexPath.row]
        }
        }
    }
}

