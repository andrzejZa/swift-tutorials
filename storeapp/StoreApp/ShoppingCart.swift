//
//  ShoppingCart.swift
//  StoreApp
//
//  Created by stud on 08/01/2018.
//  Copyright © 2018 al. All rights reserved.
//

import UIKit

class ShoppingCart {
    static var albums: [Album] = [Album]()
    
    
    static func getPrice() -> Float {
        var price : Float = 0.0
        for album in albums {
            price += album.price
        }
        return price
    }
}
