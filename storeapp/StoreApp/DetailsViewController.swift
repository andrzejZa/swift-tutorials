//
//  DetailsViewController.swift
//  StoreApp
//
//  Created by stud on 08/01/2018.
//  Copyright © 2018 al. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {
    
    @IBOutlet var imageView : UIImageView!
    @IBOutlet var priceView : UITextField!
    @IBOutlet var nameView: UITextField!
    
    @IBOutlet var shoppingCart: UITextField!
    
    
    
    var selected = Album()
    
    @objc override func viewDidLoad() {
        imageView.image = UIImage(named: selected.image)
        priceView.text = selected.price.description + "$"
        nameView.text = selected.name
        
        shoppingCart.text = ShoppingCart.getPrice().description + "$"
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
       ShoppingCart.albums.append(selected)
       shoppingCart.text = ShoppingCart.getPrice().description + "$"
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "backToCat") {
            let destination = segue.destination as! GenreTableViewController;
            destination.id = selected.genre
        } else if (segue.identifier == "showCart") {
            let destination1 = segue.destination as! ShoppingCartTableViewController;
            destination1.backAlbum = selected
        }
    }
    

}
