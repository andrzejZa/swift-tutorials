//
//  GenreTableViewController.swift
//  StoreApp
//
//  Created by stud on 03/01/2018.
//  Copyright © 2018 al. All rights reserved.
//

import UIKit

class GenreTableViewController: UITableViewController {
    
    var id = 0;
    
    @IBOutlet var nav : UINavigationBar!
    
    var classicList: [String] = ["Led Zeppelin", "Jimi Hendrix", "Pink Floyd", "Jefferson Airplane", "Bob Dylan", "Johnny Cash", "Deep purple", "Rolling Stones", "The Beatles", "Jimi Hendrix", "Pink Floyd", "Jefferson Airplane", "Bob Dylan", "Johnny Cash", "Deep purple", "Rolling Stones", "The Beatles"]
    
    var popList: [String] = ["Beyonce", "Madonna", "Katy Perry", "Chris Allen", "Akon", "Nicky Minaj", "Marc Antony", "Auburn", "Chris Atkins", "Clay Aiken", "Michael Alig"]
    
    var heavyList: [String] = ["Swans", "Metallica", "Anthrax", "Megadeth", "Slayer", "Godflesh", "Mastodon", "Russel Arms", "Chris Ayler"]
    
    var electroList: [String] = ["Chemical Brothers", "Hardwell", "Cone", "Renan Hard", "Trinity Sound System", "Mario Bros"]
    
    var usedList: [String] = ["empty"]
    
    var image: String = "empty"
    
    var priceList: [Float] = [ 4.5, 4.5, 6, 5.3, 6.3, 8.5, 6.0, 3.3, 5.5, 4.4, 8.0, 9.0, 10.0, 7.45, 2.25, 6.25, 7.99, 4.99, 4.77, 3.20, 8.55, 9.99, 7.55, 4.55 ]
    
    var selected = Album()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (id == 1) {
            usedList = classicList
            image = "classic"
            nav.topItem?.title = "Classic rock"
        } else if (id == 2) {
            usedList = heavyList
            image = "heavy"
            nav.topItem?.title = "Heavy metal"
        } else if (id == 3) {
            usedList = popList
            image = "pop"
            nav.topItem?.title = "Pop"
        } else if (id == 4) {
            usedList = electroList
            image = "electro"
            nav.topItem?.title = "Electronic"
        }
        
        selected.image = image;
        
        print(usedList);
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usedList.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)

        cell.textLabel?.text = usedList[indexPath.row]
        cell.imageView?.image = UIImage(named: image)
        
        return cell
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "detailsSegue") {
            let row = tableView.indexPathForSelectedRow?.row
            
            selected.name = usedList[row!]
            selected.price = priceList[row!]
            selected.genre = id
            let destination = segue.destination as! DetailsViewController;
            destination.selected = selected
        }
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */ 

}
