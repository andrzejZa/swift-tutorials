//
//  ViewController.swift
//  StoreApp
//
//  Created by stud on 03/01/2018.
//  Copyright © 2018 al. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destination = segue.destination as! GenreTableViewController;
        
        if segue.identifier == "classic" {
            destination.id = 1;
        } else if segue.identifier == "heavy" {
            destination.id = 2;
        } else if segue.identifier == "pop" {
            destination.id = 3;
        } else if segue.identifier == "electro" {
            destination.id = 4;
        }
    }
    
}

