//
//  ViewController.swift
//  Lab5Swift
//
//  Created by stud on 17.12.2017.
//  Copyright © 2017 stud. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDataSource, UITableViewDelegate  {
    var restaurantNames = ["Pod papugami","Naleśniki babci","Bar Nadwaga","Hortyca","Restauracja pod gryfami", "Spiż","Kurna Chata","Przystanek Wrocek","Pierogarnia Smacznego", "Mega Pierożki", "Bar Narożniak", "Bar Borowik","Pizzeria Papu","Pizzeria Etna", "Restauracja Lviv","Pierogarnia", "Pijalnia vódli i piwa"]
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return restaurantNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "Cell"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier , for: indexPath)
        cell.textLabel?.text = restaurantNames[indexPath.row]
        cell.imageView?.image = UIImage(named: "restaurant")
        return cell
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    override var prefersStatusBarHidden: Bool{
        return true
    }

}

