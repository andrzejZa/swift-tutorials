//
//  BookDetailViewController.swift
//  Sklep
//
//  Created by Andrzej Z on 06.03.2018.
//  Copyright © 2018 wso. All rights reserved.
//

import UIKit

class BookDetailViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {
      @IBOutlet weak var tableView: UITableView!
    @IBOutlet var headerView:DetailImageView!
    weak var book:Book?
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        switch indexPath.row {
//        case 0:
//            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing:DetailImageView.self), for: indexPath) as! DetailImageView
//            //let cell = tableView.dequeueReusableCell(withIdentifier: String(describing:DetailImageView.self), for: indexPath)
//           cell.pictureView.image = UIImage(named:book!.Photo)
//            return cell
      
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing:DetailTextView.self), for: indexPath) as! DetailTextView
            cell.titleLabel.text = book!.Title
            cell.authorLabel.text = book!.Author
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing:DetailCellView.self), for: indexPath) as! DetailCellView
            cell.descriptionLabel.text = book!.Description
            return cell
        default:
            fatalError("nop")
            
        
        }
        
    }
    

    
   
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
         headerView.pictureView.image = UIImage(named:book!.Photo)
       // tableView.delegate = self
      //  tableView.dataSource = self
//        self.tableView.register(DetailImageView.self, forCellReuseIdentifier: "DetailImageView")
//        self.tableView.register(DetailTextView.self, forCellReuseIdentifier: "DetailTextView")
//        self.tableView.register(DetailCellView.self, forCellReuseIdentifier: "DetailCellView")

        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
