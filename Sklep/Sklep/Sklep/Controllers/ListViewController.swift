//
//  ListViewController.swift
//  Sklep
//
//  Created by stud on 1/27/18.
//  Copyright © 2018 wso. All rights reserved.
//

import UIKit

class ListViewController: UIViewController,UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    var category:Category = Category.SWIFT;
    var id=0
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return BookReposiory.Get().GetBooks(category).count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let book = BookReposiory.Get().GetBooks(category)[indexPath.row]
        let cellIdentifier = "Cell"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier , for: indexPath) as! SklepTableViewCell
//        if category == "OBJC"
//        {
        //cell.textLabel?.text = book.Title
        cell.title?.numberOfLines=0
        cell.title?.text = book.Title
        cell.author?.text = book.Author
        cell.picture?.image = UIImage(named: book.Photo)
        //cell.imageView?.image = UIImage(named: book.Photo)
//        } else {
//            cell.textLabel?.text = restaurantImages[indexPath.row]
//            cell.imageView?.image = UIImage(named: "restaurant")
//        }
        return cell
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override var prefersStatusBarHidden: Bool{
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showBookDetail"{
            if let indexPath = tableView.indexPathForSelectedRow{
                let destController = segue.destination as! BookDetailViewController
                destController.book = BookReposiory.Get().GetBooks(category)[indexPath.row]
            }
        }
    }

}
