//
//  Book.swift
//  Sklep
//
//  Created by Andrzej Z on 25.02.2018.
//  Copyright © 2018 wso. All rights reserved.
//

import UIKit

class Book {
    var Title: String!
    var Description: String!
    var Author: String!
    var Category:Category
    var Photo:String!
    var Price:Double!
    init(title:String, description:String, author:String, category:Category, photo:String) {
        Title = title
        Description = description
        Author = author
        Category = category
        Photo = photo
    }
    
    
}
enum Category {
    case UNKNOWN
    case SWIFT
    case OBJC
    case JAVA
    case ANDROID
}
