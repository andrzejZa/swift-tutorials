//
//  BookReposiory.swift
//  Sklep
//
//  Created by Andrzej Z on 06.03.2018.
//  Copyright © 2018 wso. All rights reserved.
//

import UIKit

class BookReposiory: NSObject {
    static var bookRepository:BookReposiory = BookReposiory()
    static func Get()->BookReposiory{
        return bookRepository;
    }
    var Books = [Book]();
    override init() {
        
        Books.append(Book(title:"Swift od podstaw. Praktyczny przewodnik",
                          description:"Firma Apple od lat z powodzeniem i świetnym wyczuciem potrzeb klientów działa na rynku elektroniki użytkowej. Pracujący w niej inżynierowie projektują produkty oryginalne, funkcjonalne i wyprzedzające swoją epokę. Ważną składową tego sukcesu jest wysokiej jakości oprogramowanie, które można tworzyć wyłącznie za pomocą wydajnych, wygodnych w użyciu i nowoczesnych narzędzi programistycznych. Przewodnik ten poświęcono językowi Swift, który jest podstawą oprogramowania współpracującego z macOS, iOS, watchOS oraz tvOS. Od chwili wprowadzenia na rynek w 2014 roku Swift doczekał się już trzeciej wersji i powoli wypiera królujący niepodzielnie od lat Objective-C.",
                           author:"Paweł Pasternak",
                          category:Category.SWIFT,
                        photo:"swift2"
                          ))
        Books.append(Book(title:"Programowanie w języku Swift. Big Nerd Ranch Guide",
                          description:"Swift, następca języka Objective-C, pojawił się na rynku w 2014 roku i bardzo szybko zyskuje popularność wśród programistów tworzących aplikacje dla iOS oraz macOS. Charakteryzuje się zwięzłą i przejrzystą składnią, a co więcej, pozwala na korzystanie z nowoczesnych, wysokopoziomowych struktur i cech języka, takich jak typy generyczne czy domknięcia. Jest przy tym wygodny i elastyczny, a jego nauka nie powinna sprawiać problemów nawet tym, którzy dopiero rozpoczynają swoją przygodę z programowaniem.",
                          author:"Matthew Mathias, John Gallagher",
                          category:Category.SWIFT,
                          photo:"swift1"
        ))
        Books.append(Book(title:"Podstawy języka Swift. Programowanie aplikacji dla platformy iOS",
                          description:"Język Swift, uważany za następcę Objective-C, bardzo szybko stał popularny wśród programistów tworzących aplikacje na platformę iOS. Dzieje się tak z wielu przyczyn: Swift w porównaniu ze swoim poprzednikiem charakteryzuje się dużo bardziej zwięzłą i przejrzystą składnią, a kod napisany w tym języku jest znacznie krótszy i czytelniejszy. Ponadto Swift łączy wiele elementów takich języków, jak: JavaScript, Python, Ruby czy C#, dzięki czemu osoby posługujące się tymi językami nie powinny mieć trudności z opanowaniem Swifta. Swift jest językiem kompilowanym o dużej wydajności, z kompilatorem typu LLVM (ang. Low Level Virtual Machine). Oferuje on liczne funkcje wspomagające programowanie, przy tym jest elastyczny, jeśli chodzi o typy danych. Dzięki swej strukturze umożliwia wykorzystywanie paradygmatu programowania funkcyjnego.",
                          author:"Mark A. Lassoff",
                          category:Category.SWIFT,
                          photo:"swift3"
        ))
        Books.append(Book(title:"Learning Swift. Building Apps for macOS, iOS, and Beyond.",
                          description:"Get valuable hands-on experience with Swift 3, the latest version of Apple’s programming language. With this practical guide, skilled programmers with little or no knowledge of Apple development will learn how to code with Swift 3 by developing three complete, tightly linked versions of the Notes application for the OS X, iOS, and watchOS platforms.",
                          author:"Paris Buttfield-Addison, Jon Manning, Tim Nugent",
                          category:Category.SWIFT,
                          photo:"swift4"
        ))
        Books.append(Book(title:"Swift Development for the Apple Watch. An Intro to the WatchKit Framework, Glances, and Notifications",
                          description:"Apple Watch is the sort of science-fiction gadget that people used to dream about as kids. What kinds of apps do you envision for this new device? If you’re comfortable using OS X, Xcode, and iOS—and familiar with Swift—this concise book shows you the basics of building your own apps for this wrist-mounted computer with Apple’s WatchKit framework. You’ll learn what an Apple Watch is, what it isn’t, and how and why people might interact with apps you build for it. This practical guide also examines the type of apps most suitable for this device, and shows you how to be a good citizen in the iOS/Watch ecosystem.",
                          author:"Jon Manning, Paris Buttfield-Addison",
                          category:Category.SWIFT,
                          photo:"swift5"
        ))
        Books.append(Book(title:"iOS Swift Game Development Cookbook. Simple Solutions for Game Development Problems.",
                          description:"Ready to make amazing games for the iPhone, iPad, and iPod touch? With Apple’s Swift programming language, it’s never been easier. This updated cookbook provides detailed recipes for a managing wide range of common iOS game-development issues, ranging from 2D and 3D math to SpriteKit and OpenGL to performance—all revised for Swift. You get simple, direct solutions to common problems found in iOS game programming. Need to figure out how to give objects physical motion, or want a refresher on gaming-related math problems? This book provides sample projects and straightforward answers. All you need to get started is some familiarity with iOS development, Swift, and Objective-C.",
                          author:"Jonathon Manning, Paris Buttfield-Addison",
                          category:Category.SWIFT,
                          photo:"swift6"
        ))
        Books.append(Book(title:"Swift Development with Cocoa. Developing for the Mac and iOS App Stores",
                          description:"Ready to build apps for iPhone, iPad, and Mac now that Swift has landed? If you’re an experienced programmer who’s never touched Apple developer tools, this hands-on book shows you how to use the Swift language to make incredible iOS and OS X apps, using Cocoa and Cocoa Touch. Learn how to use Swift in a wide range of real-world situations, with Cocoa features such as Event Kit and Core Animation. You’ll pick up Swift language features and syntax along the way, and understand why using Swift (instead of Objective-C) makes iOS and Mac app development easier, faster, and safer. You’ll also work with several exercises to help you practice as you learn.",
                          author:"Jonathon Manning, Paris Buttfield-Addison, Tim Nugent",
                          category:Category.SWIFT,
                          photo:"swift7"
        ))
        Books.append(Book(title:"Objective-C. Leksykon profesjonalisty",
                          description:"Objective-C to nowoczesny język programowania, dzięki któremu możesz tworzyć zaawansowane aplikacje dla produktów ze stajni Apple. Produkty takie, jak iPad, iPhone czy laptopy z systemem operacyjnym MacOS, podbiły serca użytkowników na całym świecie. Co ważne, ich pozycja wydaje się niezagrożona! Dlatego inwestycja w wiedzę na temat tego języka jest w pełni uzasadniona. Z tą książką błyskawicznie poznasz możliwości języka Objective-C. Dzięki przystępnemu wprowadzeniu zapoznasz się z podstawami języka, a w kolejnych rozdziałach poszerzysz wiedzę o bardziej zaawansowane zagadnienia. Podręcznik wypełniony ponad setką listingów z kodem źródłowym programów sprawi, że będziesz mógł stworzyć działający kod w języku Objective-C praktycznie w każdej sytuacji. W trakcie lektury dowiesz się, jak zarządzać pamięcią, korzystać ze wzorców oraz wykonywać operacje na ciągach znaków, liczbach i kolekcjach. Ponadto sprawdzisz, jak w Objective-C korzystać z plików, wątków i dostępu do sieci. Poświęć chwilę tej książce, a już wkrótce zaczniesz tworzyć zaawansowane oprogramowanie w Objective-C!",
                          author:"David Chisnall",
                          category:Category.OBJC,
                          photo:"objc1"
        ))
        Books.append(Book(title:"Objective-C. Vademecum profesjonalisty. Wydanie III",
                          description:"Tak jak iPhone, iPad czy iPod Touch błyskawicznie stały się obiektem pożądania milionów ludzi na całym świecie, tak samo szybko rynek upomniał się o specjalistów od tworzenia aplikacji na te innowacyjne urządzenia. Mimo że od 2007 roku, gdy Apple opublikowało zaktualizowaną wersję języka Objective-C, oznaczoną jako 2.0, minęło już trochę czasu, programistów znających ten język wciąż jest niewielu, a zapotrzebowanie na programy dla systemów iOS i Mac OS X stale rośnie. Warto zatem opanować ten potężny język, zarazem prosty i oferujący ogromne perspektywy zawodowe. Zwłaszcza że można go wykorzystać także na wielu innych platformach z kompilatorem gcc, a więc między innymi w systemach Unix, Linux i Windows.",
                          author:"Stephen G. Kochan",
                          category:Category.OBJC,
                          photo:"objc2"
        ))
        Books.append(Book(title:"Objective-C. Podstawy",
                          description:"Jeszcze parę lat temu nie do pomyślenia było, że aplikacje na urządzenia mobilne mogą stanowić tak intratny interes. Jednak urządzenia te podbiły rynek w mgnieniu oka i dziś trudno wyobrazić sobie życie bez nich. Co więcej, dały one jeszcze większe możliwości działania różnym projektantom - praktycznie wszystkie wyposażone są w aparat fotograficzny, odbiornik GPS oraz czujniki położenia. To peryferia, o których programiści tworzący aplikacje na standardowe komputery mogą tylko pomarzyć. Zastanawiasz się, jak wykorzystać ten potencjał? Ta książka dostarczy Ci odpowiedzi. W trakcie lektury nauczysz się tworzyć atrakcyjne aplikacje na platformę iOS. Jest ona wykorzystywana w urządzeniach firmy Apple, których nikomu nie trzeba przedstawiać. Podczas tworzenia aplikacji dla tej platformy będziesz korzystał z języka Objective-C oraz środowiska XCode 4. Zawarta tu wiedza i liczne przykłady krok-po-kroku pozwolą Ci błyskawicznie opanować trudniejsze partie materiału. Książka ta jest idealną pozycją dla wszystkich programistów chcących rozpocząć przygodę z platformą iOS.",
                          author:"Christopher Fairbairn, Collin Ruffenach, Johannes Fahrenkrug",
                          category:Category.OBJC,
                          photo:"objc3"
        ))
        Books.append(Book(title:"Learning Cocoa with Objective-C. Developing for the Mac and iOS App Stores.",
                          description:"Get up to speed on Cocoa and Objective-C, and start developing applications on the iOS and OS X platforms. If you don’t have experience with Apple’s developer tools, no problem! From object-oriented programming to storing app data in iCloud, the fourth edition of this book covers everything you need to build apps for the iPhone, iPad, and Mac. You’ll learn how to work with the Xcode IDE, Objective-C’s Foundation library, and other developer tools such as Event Kit framework and Core Animation. Along the way, you’ll build example projects, including a simple Objective-C application, a custom view, a simple video player application, and an app that displays calendar events for the user.",
                          author:"Paris Buttfield-Addison, Jonathon Manning, Tim Nugent",
                          category:Category.OBJC,
                          photo:"objc4"
        ))
        Books.append(Book(title:"iOS 7 Programming Fundamentals. Objective-C, Xcode, and Cocoa Basics",
                          description:"If you’re getting started with iOS development, or want a firmer grasp of the basics, this practical guide provides a clear view of its fundamental building blocks—Objective-C, Xcode, and Cocoa Touch. You’ll learn object-oriented concepts, understand how to use Apple’s development tools, and discover how Cocoa provides the underlying functionality iOS apps need to have. Dozens of example projects are available at GitHub. Once you master the fundamentals, you’ll be ready to tackle the details of iOS app development with author Matt Neuburg’s companion guide Programming iOS 7.",
                          author:"Matt Neuburg",
                          category:Category.OBJC,
                          photo:"objc5"
        ))
        Books.append(Book(title:"Cocoa and Objective-C: Up and Running",
                          description:"Build solid applications for Mac OS X, iPhone, and iPod Touch, regardless of whether you have basic programming skills or years of programming experience. With this book, you'll learn how to use Apple's Cocoa framework and the Objective-C language through step-by-step tutorials, hands-on exercises, clear examples, and sound advice from a Cocoa expert. Cocoa and Objective-C: Up and Running offers just enough theory to ground you, then shows you how to use Apple's rapid development tools -- Xcode and Interface Builder -- to develop Cocoa applications, manage user interaction, create great UIs, and more. You'll quickly gain the experience you need to develop sophisticated Apple software, whether you're somewhat new to programming or just new to this platform.",
                          author:"Scott Stevenson",
                          category:Category.SWIFT,
                          photo:"objc6"
        ))
        Books.append(Book(title:"Objective-C Pocket Reference",
                          description:"Objective-C is an exciting and dynamic approach to C-based object-oriented programming; it's the approach adopted by Apple as the foundation for programming under Mac OS X, a Unix-based operating system gaining wide acceptance among programmers and other technologists. Objective-C is easy to learn and has a simple elegance that is a welcome breath of fresh air after the abstruse and confusing C++. To help you master the fundamentals of this language, you'll want to keep the Objective-C Pocket Reference close at hand. This small book contains a wealth of valuable information to speed you over the learning curve.In this pocket reference, author Andrew Duncan provides a quick and concise introduction to Objective-C for the experienced programmer. In addition to covering the essentials of Objective-C syntax, Andrew also covers important faces of the language such as memory management, the Objective-C runtime, dynamic loading, distributed objects, and exception handling.O'Reilly's Pocket References have become a favorite among programmers everywhere. By providing important details in a succinct, well-organized format, these handy books deliver just what you need to complete the task at hand. When you've reached a sticking point in your work and need to get to a solution quickly, the new Objective-C Pocket Reference is the book you'll want to have.",
                          author:"Andrew Duncan",
                          category:Category.OBJC,
                          photo:"objc7"
        ))
        Books.append(Book(title:"",
                          description:"",
                          author:"",
                          category:Category.SWIFT,
                          photo:"swift"
        ))
    }
    
    func GetBooks(_ category:Category) -> [Book]{
        var bookArray=[Book]()
        for book in Books{
            if book.Category == category {
                bookArray.append(book);
            }
        }
        return bookArray
    }
}
