//
//  DetailCellView.swift
//  Sklep
//
//  Created by Andrzej Z on 07.03.2018.
//  Copyright © 2018 wso. All rights reserved.
//

import UIKit

class DetailCellView: UITableViewCell {
    @IBOutlet var descriptionLabel: UILabel!{
        didSet{
            descriptionLabel.numberOfLines=0;
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
