//
//  SklepTableViewCell.swift
//  Sklep
//
//  Created by Andrzej Z on 21.02.2018.
//  Copyright © 2018 wso. All rights reserved.
//

import UIKit

class SklepTableViewCell: UITableViewCell {
    @IBOutlet var title: UILabel!
    @IBOutlet var locationLabel:UILabel!
    @IBOutlet var typeLabel: UILabel!
    @IBOutlet var picture :UIImageView!
    @IBOutlet var author:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
