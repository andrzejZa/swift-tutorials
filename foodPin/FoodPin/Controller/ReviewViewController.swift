//
//  ReviewViewController.swift
//  FoodPin
//
//  Created by stud on 10/03/2018.
//  Copyright © 2018 AppCoda. All rights reserved.
//

import UIKit

class ReviewViewController: UIViewController {
    @IBOutlet var backgroundImageView: UIImageView!
    @IBOutlet var rateButtons: [UIButton]!
    @IBOutlet var closeButton: UIButton!
    
    var restaurant = Restaurant()
    override func viewDidLoad() {
        super.viewDidLoad()
        backgroundImageView.image = UIImage(named: restaurant.image)
        let blurEffect = UIBlurEffect(style: .dark)
        let blurEffectView = UIVisualEffectView(effect:blurEffect)
        blurEffectView.frame = view.bounds
        backgroundImageView.addSubview(blurEffectView)
        // Do any additional setup after loading the view.
        
        let slideInFromTop = CGAffineTransform.init(translationX: 0, y: -80 )
        
        let moveRightTransform = CGAffineTransform.init(translationX: 600, y: 0)
        let scaleUptransform = CGAffineTransform.init(scaleX: 5.0, y: 5.0)
        let combineTransform = scaleUptransform.concatenating(moveRightTransform)
        for button in rateButtons{
            button.transform = combineTransform
            button.alpha = 0
        }
        closeButton.transform = slideInFromTop
        closeButton.alpha = 0
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    override func viewWillAppear(_ animated: Bool) {
        var _delay = 0.1
        for button in self.rateButtons{
            
            UIView.animate(withDuration: 0.4, delay: _delay, options: [], animations:{
                button.alpha = 1.0
                button.transform = .identity
            })
            _delay += 0.05
        }
        UIView.animate(withDuration: 0.7, delay: _delay, options: [], animations:{
            self.closeButton.alpha = 1
            self.closeButton.transform = .identity
        })
//        UIView.animate(withDuration: 2.0){
//            for button in self.rateButtons {
//                button.alpha = 1
//            }
//        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
