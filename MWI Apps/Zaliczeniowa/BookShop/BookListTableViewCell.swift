//
//  BookListTableViewCell.swift
//  BookShop
//
//  Created by Weronika Piotrowska on 10.01.2018.
//  Copyright © 2018 MWI. All rights reserved.
//

import UIKit

class BookListTableViewCell: UITableViewCell {

    @IBOutlet var titleLabel : UILabel!
    @IBOutlet var priceLabel : UILabel!
    @IBOutlet var bookImageView : UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
