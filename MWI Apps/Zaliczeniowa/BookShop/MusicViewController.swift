//
//  MusicViewController.swift
//  BookShop
//
//  Created by Weronika Piotrowska on 11.01.2018.
//  Copyright © 2018 MWI. All rights reserved.
//

import UIKit
import AVFoundation

class MusicViewController: UIViewController {

    @IBDesignable class UITextViewFixed : UITextView {
        override func layoutSubviews() {
            super.layoutSubviews()
            setup()
        }
        func setup() {
            textContainerInset = UIEdgeInsets.zero
            textContainer.lineFragmentPadding = 0
        }
    }
    
    @IBAction func showMessage(sender: UIButton) {
        globalPrice = globalPrice + price
        globalBookPrices = globalBookPrices + [price]
        globalBookNames = globalBookNames + [testName]
        
        sumPriceButton.setTitle("SUMA: " + globalPrice.description + " zl", for: [])
        let alertController = UIAlertController(title: "Ksiazke dodano do koszyka", message: "Aby wyswietlic stan produktow w koszyku, nacisnij na gorny pasek z aktualnym stanem twojego konta.", preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
    
    
    @IBOutlet var testImageView: UIImageView!
    //    @IBOutlet var iconImageView : UIImageView!
    @IBOutlet var priceLabel : UILabel!
    @IBOutlet var sumPriceButton : UIButton!
    
    var testName = ""
    var musicFileName = ""
    var testImageName = ""
    var price = 0.0
    
    
    var songPlayer = AVAudioPlayer()
    
    func prepareSondAndSession() {
        do {
            songPlayer = try AVAudioPlayer(contentsOf: URL.init(fileURLWithPath: Bundle.main.path(forResource: "koledy_swiata", ofType:"mp3")!))
            songPlayer.prepareToPlay()
        
            let audioSession = AVAudioSession.sharedInstance()
            do {
                try audioSession.setCategory(AVAudioSessionCategoryPlayback)
            } catch let sessionError {
                print(sessionError)
            }
            
        } catch let songPlayerError {
            print(songPlayerError)
        }
        
    }
    
    @IBAction func play(_ sender: Any) {
//        songPlayer.play()
    }
    
    @IBAction func pause(_ sender: Any) {
//        songPlayer.pause()
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        prepareSondAndSession()
        
        // Do any additional setup after loading the view.
        testImageView.image = UIImage(named: testImageName)
        priceLabel.text = price.description + " zl"
        sumPriceButton.setTitle("SUMA: " + globalPrice.description + " zl", for: [])
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
 
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    


}
