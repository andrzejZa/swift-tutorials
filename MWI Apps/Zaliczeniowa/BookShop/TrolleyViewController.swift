//
//  TrolleyViewController.swift
//  BookShop
//
//  Created by Weronika Piotrowska on 11.01.2018.
//  Copyright © 2018 MWI. All rights reserved.
//

import UIKit

class TrolleyViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet var sumpriceLabel : UILabel!
//    var bookNames = [String]()
//    var prices = [Double]()
    
    
    
    func myButtonDelete(sender : UIButton!) {
        
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            globalBookNames.remove(at: indexPath.row)
            globalPrice = globalPrice - globalBookPrices[indexPath.row]
            globalBookPrices.remove(at: indexPath.row)
            
            tableView.reloadData()
        }
    }
  
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return globalBookNames.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "Cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
                
        cell.textLabel?.text = globalBookNames[indexPath.row]
        cell.detailTextLabel?.text = (globalBookPrices[indexPath.row]).description + " zl"
        
        
        
        return cell
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        sumpriceLabel?.text = globalPrice.description + " zl"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
