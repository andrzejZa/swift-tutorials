//
//  ViewController.swift
//  BookShop
//
//  Created by stud on 08.01.2018.
//  Copyright © 2018 MWI. All rights reserved.
//

import UIKit

var globalPrice = 0.0
var globalBookNames = [String]()
var globalBookPrices = [Double]()


class ViewController: UIViewController {

    var myImageName = "homei"
    
    var krymBookNames = ["Dzieci gniewu", "Pokuta", "W linii prostej", "Szosty"]
    var krymImageNames = ["dzieci_gniewu", "pokuta", "w_linii_prostej", "szosty"]
    var krymPrices = [40.00, 30.56, 20.34, 24.43]
    var krymBookDescriptions = ["Którzyście byli umarli w upadkach i w grzechach… z przyrodzenia dziećmi gniewu. – List do Efezjan (2:1,3) W Stanach Zjednoczonych wybucha wielki kryzys, a Berlin musi radzić sobie z własnymi problemami. Oto bowiem w mieście wybucha epidemia zachorowań.", "W pewien upalny ranek 1935 roku trzynastoletnia Briony Tallis jest przypadkowym świadkiem sceny miłosnej pomiędzy młodym Robbinem a swoją starszą siostrą. Wyobraźnia dziewczynki podsuwa jej różne interpretacje tego wydarzenia, co prowadzi do tragedii.", "Jake Fayter odpadł od skały i runął w dół z wysokości kilkudziesięciu metrów. Jego śmierć stanowiła zagadkę, której do tej pory nikt nie rozwikłał. Był świetnym wspinaczem, który skrupulatnie planował każdy krok i drobiazgowo rozpracowywał każdą trudniejszą trasę.", "Unikatowa pozycja wśród książek Edgara Wallace’a. Napisana w 1927 r. i wydana w Polsce tylko raz osiem lat później. Ekranizowana w 1962 r. Cezar Valentine jest milionerem podejrzewanym o liczne zbrodnie. Do jego zdemaskowania Scotland Yard zatrudnia Numer Szósty, agenta, którego nikt nie zna poza szefem organizacji."]
    
    var przygBookNames = ["Pan samochodzik", "Felix, Net i Nika", "Arka Ognia", "Boginii oceanu", "Igrzyska Smierci", "Wyspa Zloczyncow"]
    var przygImageNames = ["pan_samochodzik", "felix_net_nika", "arka_ognia", "bogini_oceanu", "igrzyska_smierci", "wyspa_zloczyncow"]
    var przygPrices = [43.00, 30.78, 75.24, 54.43, 67.91, 34.99]
    var przygBookDescriptions = ["Pan Tomasz dziedziczy po wujku wynalazcy murowany garaż wraz z przechowywanym w nim pokracznym samochodem. Mimo brzydoty pojazdu, zaciekawiony dziwnymi właściwościami samochodu, postanawia go zatrzymać. Wyrusza w okolice Ciechocinka na poszukiwania ukrytych tam podczas drugiej wojny zbiorów muzealnych.", "Co się stanie, jeżeli roboty uznają, że należą im się takie same prawa jak ludziom? Jak Felix, Net i Nika poradzą sobie z buntem maszyn? Jak zwykle w serii 'Felix, Net i Nika' czekają na Was: megadawka humoru, absolutnie szalone przygody i... nowe pomysły dyrektora gimnazjum.", "Znakomita współczesna powieść sensacyjno-przygodowa, akcja przenosi się z miejsca na miejsce. Ponad siedemset lat temu w wiosce Megiddo wykopano z ziemi skrzynię wykonaną ze szczerego złota. Skrzynia ta zniknęła niebawem w tajemniczych okolicznościach.", "Niezwykły portret kobiety, która daleko wyprzedziła swoje czasy Bronisława Niżyńska dorastała w cieniu brata. Choć oboje byli cudownymi dziećmi baletu, to jego okrzyknięto legendą. To jego nazywano 'Bogiem tańca'. Bronia godziła się na bycie 'tą drugą'.", "Czy zdołałbyś przetrwać w dziczy, zdany na własne siły, gdyby wszyscy dookoła próbowali wykończyć cię za wszelką cenę? Na ruinach dawnej Ameryki Północnej rozciąga się państwo Panem, z imponującym Kapitolem otoczonym przez dwanaście dystryktów.", "Niezwykle szybkim pojazdem pan Tomasz wyruszył w Polskę, aby rozszyfrować kolejną historyczną zagadkę, schwytać złodziei i przemytników usiłujących wykraść cenne zabytkowe przedmioty."]
    
    var sfBookNames = ["Stacja jedenascie", "Niezwyciezony", "Wschodzace gwiazdy", "Wehikul czasu"]
    var sfImageNames = ["stacja_jedenascie", "niezwyciezony", "wschodzace_gwiazdy", "wehikul_czasu"]
    var sfPrices = [30.21, 45.62, 21.99, 24.0]
    var sfBookDescriptions = ["Jedno z największych literackich wydarzeń roku! Obsypana nagrodami powieść, która wzrusza i zmusza do zastanowienia nad ulotnym pięknem tego świata. Zdobywca nagrody Arthura C. Clarke'a za najlepszą powieść roku. ", "Powieść Niezwyciężony zajmuje szczególne miejsce w dorobku Stanisława Lema. Z kilku powodów. Przede wszystkim jest to batalistyczna opowieść o starciu ludzi z powstałą samorzutnie na odległej planecie populacją mikroautomatów niszczących wszelkie myślenie.", "Gotowe do wojny floty grupują się wokół Darien. Wszystkie mają ten sam cel. Pragną przejąć kontrolę nad nowo odkrytą planetą i zyskać dostęp do potężnej broni w jej centrum.", "Podróżnik w Czasie po kilku latach ciężkiej pracy konstruuje tytułowy Wehikuł Czasu i wyrusza w podróż w przyszłość. Zatrzymuje się w roku 802 701, gdzie poznaje dwa gatunki, na jakie podzieliła się ludzkość: Elojów i Morloków."]
    
    var turBookNames = ["Wybrzeze Baltyku", "Armenia", "Mongolia", "Wilno", "Lodz"]
    var turImageNames = ["wybrzeze_baltyku", "armenia", "mongolia", "wilno", "lodz"]
    var turPrices = [60.41, 45.60, 78.90, 56.24, 48.97]
    var turBookDescriptions = ["Travelbook to Twój niezastąpiony towarzysz podróży. Wskaże Ci najważniejsze atrakcje, podpowie, czego szukać poza głównymi szlakami i wprowadzi w świat miejscowych obyczajów. Znajdziesz w nim opisy najciekawszych regionów i miast, a sprawdzone informacje praktyczne umożliwią staranne zaplanowanie podróży.", "Armenia. Niegdyś potężne imperium rozciągające się od Morza Kaspijskiego po Morze Śródziemne, dzisiaj zubożałe państewko na Kaukazie Południowym. A przecież to tutaj, na szczycie Araratu, osiadła po biblijnym potopie arka Noego, to tutaj na nowo miał się zacząć świat.", "Mapa samochodowa Freytag & Berndt to świetny towarzysz wyjazdów zarówno dla tych, którzy wybierają się na urlop wraz z biurem podróży, jak i dla wojażujących na własną rękę. Dzięki niej bez wpadek zaplanujesz wakacje i udany pobyt na miejscu.", "Wilno to leżąca niemal w geograficznym środku Europy stolica Litwy, a zarazem miejsce bliskie wielu pokoleniom Polaków. Dzisiejsze miasto, choć podszyte wielowiekową, wielokulturową historią, to nowoczesny ośrodek tętniący życiem w dzień i w nocy.", "Łódź to na turystycznej mapie Polski niemal terra incognita. Dawna wielokulturowa stolica włókiennictwa w Królestwie Polskim, rozsławiona przez Reymonta „ziemia obiecana” Niemców, Polaków, Żydów i Rosjan, jest dziś miastem coraz bardziej świadomym swojej tożsamości."]
    
    var progBookNames = ["Python od podstaw", "HTML 5", "Scratch bez tajemnic", "Jezyk C++", "Delphi", "Java", "PHP5"]
    var progImageNames = ["python", "html5", "scratch_bez_tajemnic", "jezyk_cpp", "delphi", "java", "php5"]
    var progPrices = [60.49, 75.20, 58.90, 76.22, 47.90, 65.43, 87.40]
    var progBookDescriptions = ["Poznaj programowanie z bliska! Naucz się Pythona! Dowiedz się, jak czytać i implementować algorytmy Naucz się analizować i rozwiązywać problemy Poznaj podstawy Pythona na praktycznych przykładach Python to niezwykle wydajny i wszechstronny język programowania.", "Najlepszy podręcznik do HTML5! Chcesz tworzyć strony internetowe, które są dynamiczne, interaktywne, bogate w treści i utrzymują łączność z innymi serwisami? To dla ciebie!", "Własna gra komputerowa? To musi się udać! Podstawowe polecenia Proste problemy programistyczne i własne bloczki Twoja pierwsza gra komputerowa ze Scratchem Jeśli sięgasz po tę książkę, na pewno uwielbiasz gry komputerowe i zastanawiasz się, jak samodzielnie stworzyć własną.", "Dwutomowa publikacja dla wszystkich, którzy chcą zrozumieć, na czym polega programowanie komputerów. Zawiera treści z zakresu podstaw algorytmów, programowania strukturalnego, programowania obiektowego oraz struktur danych.", "Poznaj najlepsze techniki pisania kodu w Delphi!Klasy generyczne, metody anonimowe i atrybuty — nowe funkcjonalności języka DelphiTesty jednostkowe, poprawianie jakości kodu i wykorzystanie platformy izolacyjnejWstrzykiwanie zależności a tworzenie czytelnego, luźno sprzężonego i łatwego do testowania kodu.", "Jeśli chcesz poznać Javę, nie szukaj dalej - oto pierwsza książka techniczna z graficznym interfejsem użytkownika! Obejmuje Javę 5.0Technologie oparte na Javie są wszędzie - jeśli piszesz oprogramowanie i jeszcze nie poznałeś Javy, nadszedł czas, by ruszyć głową!Otwórz się na Javę i projektowanie obiektowe.", "Przewodnik twórcy stron i aplikacji sieciowych! PHP wraz z bazą danych MySQL oraz językiem JavaScript to potężne trio, dzięki któremu możesz zbudować aplikację internetową dowolnej wielkości."]
    
    var CDNames = ["Tropico", "Lato z radiem", "Halfway", "Latino", "TGD - koledy swiata"]
    var CDImageNames = ["tropico", "lato_z_radiem", "halfway", "latino", "koledy_swiata"]
    var CDPrices = [23.45, 80.5, 39.99, 25.4, 19.98]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "pokazWidokTest" {
            let vc = segue.destination as! TestViewController
            vc.testImageName = self.myImageName
        }
        if segue.identifier == "ListaKryminalne" {
            let ktvc = segue.destination as! BookListTableViewController
            ktvc.bookNames = self.krymBookNames
            ktvc.imageNames = self.krymImageNames
            ktvc.prices = self.krymPrices
            ktvc.bookDescriptions = self.krymBookDescriptions
        }
        if segue.identifier == "ListaPrzygodowe" {
            let ptvc = segue.destination as! BookListTableViewController
            ptvc.bookNames = self.przygBookNames
            ptvc.imageNames = self.przygImageNames
            ptvc.prices = self.przygPrices
            ptvc.bookDescriptions = self.przygBookDescriptions
        }
        if segue.identifier == "ListaScienceFiction" {
            let sftvc = segue.destination as! BookListTableViewController
            sftvc.bookNames = self.sfBookNames
            sftvc.imageNames = self.sfImageNames
            sftvc.prices = self.sfPrices
            sftvc.bookDescriptions = self.sfBookDescriptions
        }
        if segue.identifier == "ListaTurystyczne" {
            let ttvc = segue.destination as! BookListTableViewController
            ttvc.bookNames = self.turBookNames
            ttvc.imageNames = self.turImageNames
            ttvc.prices = self.turPrices
            ttvc.bookDescriptions = self.turBookDescriptions
        }
        if segue.identifier == "ListaProgramistyczne" {
            let prtvc = segue.destination as! BookListTableViewController
            prtvc.bookNames = self.progBookNames
            prtvc.imageNames = self.progImageNames
            prtvc.prices = self.progPrices
            prtvc.bookDescriptions = self.progBookDescriptions
        }
        if segue.identifier == "ListaCD" {
            let ctvc = segue.destination as! BookListTableViewController
            ctvc.bookNames = self.CDNames
            ctvc.imageNames = self.CDImageNames
            ctvc.prices = self.CDPrices
        }
    }


}

