//
//  TestViewController.swift
//  BookShop
//
//  Created by Weronika Piotrowska on 10.01.2018.
//  Copyright © 2018 MWI. All rights reserved.
//

import UIKit

//struct MyGlobalVariables {
//    var sumPrice = 0.0
//}

class TestViewController: UIViewController {

    @IBDesignable class UITextViewFixed : UITextView {
        override func layoutSubviews() {
            super.layoutSubviews()
            setup()
        }
        func setup() {
            textContainerInset = UIEdgeInsets.zero
            textContainer.lineFragmentPadding = 0
        }
    }
    
    @IBAction func showMessage(sender: UIButton) {
        globalPrice = globalPrice + price
        globalBookPrices = globalBookPrices + [price]
        globalBookNames = globalBookNames + [testName]

        sumPriceButton.setTitle("SUMA: " + globalPrice.description + " zl", for: [])
        let alertController = UIAlertController(title: "Ksiazke dodano do koszyka", message: "Aby wyswietlic stan produktow w koszyku, nacisnij na gorny pasek z aktualnym stanem twojego konta.", preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
    
    
    @IBOutlet var testImageView: UIImageView!
    @IBOutlet var testDescription : UITextViewFixed!
//    @IBOutlet var iconImageView : UIImageView!
    @IBOutlet var priceLabel : UILabel!
    @IBOutlet var sumPriceButton : UIButton!
    
    var testName = ""
    var testDesc = ""
    var testImageName = ""
    var price = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        testImageView.image = UIImage(named: testImageName)
        testDescription.text = testDesc
//        iconImageView.image = UIImage(named: "icon")
        priceLabel.text = price.description + " zl"
        sumPriceButton.setTitle("SUMA: " + globalPrice.description + " zl", for: [])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
     // MARK: - Navigation
     // In a storyboard-based application, you will often want to do a little preparation before navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "ListaKsiazek" {
//            let destinationController = segue.destination as! TrolleyViewController
//            destinationController.testImageName = "palominoespresso"
//        }
        
    }
    

}
