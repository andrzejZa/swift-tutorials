//
//  BookListTableViewController.swift
//  BookShop
//
//  Created by Weronika Piotrowska on 10.01.2018.
//  Copyright © 2018 MWI. All rights reserved.
//

import UIKit


class BookListTableViewController: UITableViewController {

    
    var bookNames = [String]() //["Dzieci gniewu", "Pokuta", "W linii prostej", "Szosty"]
    var imageNames = [String]() //["dzieci_gniewu", "pokuta", "w_linii_prostej", "szosty", "szosty"]
    var prices = [Double]() //[40.00, 30.56, 20.34, 24.43, 22.22]
    var bookDescriptions = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return bookNames.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "Cell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! BookListTableViewCell

        // Configure the cell...
        cell.titleLabel.text = bookNames[indexPath.row]
        cell.bookImageView.image = UIImage(named: imageNames[indexPath.row])
        cell.priceLabel.text = (prices[indexPath.row]).description + " zl"
        
        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Szczegoly" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let vc = segue.destination as! TestViewController
                vc.testImageName = imageNames[indexPath.row]
                vc.testDesc = bookDescriptions[indexPath.row]
                vc.testName = bookNames[indexPath.row]
                vc.price = prices[indexPath.row]
            }
        }
            
        if segue.identifier == "SzczegolyCD" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let vc = segue.destination as! MusicViewController
                vc.testImageName = imageNames[indexPath.row]
                vc.testName = bookNames[indexPath.row]
                vc.price = prices[indexPath.row]
            }
        }
    }
    

}
