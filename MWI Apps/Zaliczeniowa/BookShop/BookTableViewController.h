//
//  BookTableViewController.h
//  BookShop
//
//  Created by Weronika Piotrowska on 10.01.2018.
//  Copyright © 2018 MWI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookTableViewController : UITableViewController

@end
